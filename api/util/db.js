'use strict';

const mongoose = require('mongoose');
const promiseRetry = require('promise-retry');

const options = {
	useFindAndModify: false,
	useNewUrlParser: true,
	reconnectTries: 60,
	reconnectInterval: 1000,
	poolSize: 10,
	bufferMaxEntries: 0, // if not connected return errors immediately rather than waiting to reconnect
	useUnifiedTopology: true
};

const promiseRetryOptions = {
	retries: options.reconnectTries,
	factor: 2,
	minTimeout: options.reconnectInterval,
	maxTimeout: 5000
};

const connect = url => promiseRetry((retry, number) => (
	console.log(`MongoClient is connecting to ${url} - retry number: ${number}`),
	mongoose.connect(url, options).catch(retry)
	// mongoose.createConnection(url, options).catch(retry)
), promiseRetryOptions);

const clean = (db, name) => db.then(
	db => db.model(name).deleteMany({})
);

const creteObjectId = () => new mongoose.Types.ObjectId();

module.exports = {
	connect,
	clean,
	creteObjectId
};
