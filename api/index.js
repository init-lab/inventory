'use strict';

const status = require('http-status');

// lib
const path = require('path');

// express dep
const express = require('express');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const cors = require('cors');
const morgan = require('morgan');
// db
const dbUtil = require('./util/db');

// rest api
const api = require('iblokz-rest-api');
const apiResponse = require('iblokz-rest-api/lib/middleware/response');
const rest = require('./config/rest.js');

// init
// app
const app = express();
// express prefs
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(methodOverride());
app.use(cors());
app.use(morgan('\t \x1b[33m[>>] \x1b[37m:method :url :status :response-time ms - :res[content-length]'));
// db
const db = dbUtil.connect(process.env.MONGO_URL);

// rest api init
// loads the models from json schema
api.loadModel(rest);
// initis rest endpoints
api.initRoutes(app, rest, {});

app.route('*')
	.get((req, res) => {
		console.log(req.baseUrl, '-', req.originalUrl, '-', req.path);
		let response = {...res.body, ...(
			// don't return images in collection list requests
			req.path === '/api/items'
			? {list: res.body.list.map(item => ({...item, images: undefined}))}
			: req.path === '/api/rooms'
				? {list: res.body.list.map(item => ({...item, image: undefined}))}
				: {}
		)};
		return res.status(status.OK).json(response);
	})
	.put(apiResponse('put'))
	.post(apiResponse('post'))
	.delete(apiResponse('delete'));

// connect
const port = process.env.PORT;
app.listen(port, () => console.log(`users listening to port ${port}`));
module.exports = app;
