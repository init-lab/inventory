#!/bin/bash
# build-push.sh image branch
echo $REGISTRY_URL $1 $2 $3
[ $3 ] && cp env/${3}.env $1/.env
docker build -t $1-$2 $1
docker tag $1-$2 $REGISTRY_URL/$2/$1:latest
docker push $REGISTRY_URL/$2/$1:latest
# [ $3 ] && rm -rf $1/.env
