# prep dirs
rm -rf dist
mkdir -p dist dist/js dist/css dist/assets
# index
node_modules/.bin/pug --pretty < src/pug/index.pug > dist/index.html
# assets
node bin/move-assets.js
# js
export $(cat .env | grep -v ^# | xargs)
echo $SERVER_URL
node_modules/.bin/browserify -t envify -t [ babelify --global ] -e src/js/index.js -o dist/js/app.js --debug
# sass
sh bin/build-sass.sh
