# build first
sh ./bin/build.sh
# assets
node_modules/.bin/nodemon -e jpg,jpeg,gif,png,json,md,svg,flac,ogg --watch src/assets --exec 'node bin/move-assets.js' & \
# js
node_modules/.bin/watchify -t [ babelify --global ] -p [browserify-hmr browserify-hmr -h 0.0.0.0 -p $HMR_PORT -u "http://localhost:${HMR_PORT}"] src/js/index.js -o dist/js/app.js --debug & \
# sass
node_modules/.bin/nodemon -w ./src/sass -e sass -x 'sh bin/build-sass.sh' & \
# livereload
node_modules/.bin/livereload ./dist/**/*.css -p $LVRLD_PORT -d & \
# serve
node_modules/.bin/serve -s -l $PORT dist
