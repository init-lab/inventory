'use strict';

// dom
const {
	h1, a, div, header,
	section, button, span,
	ul, li, img, i
} = require('iblokz-snabbdom-helpers');
const {str, obj} = require('iblokz-data');

module.exports = ({state, actions, i18n = {routes: {}}}) => header([
	h1(a('[href="#/"]', [
		img(`[src="assets/logo.png"]`),
		// i('.fa.fa-puzzle-piece'),
		span(' init Lab Inventory')
	])),
	ul('.menu', Object.keys(state.rest.routes.api)
		.filter(route => route !== '_meta')
		.map(route =>
			li([a(
				`[href="#/${route === 'home' ? '' : route}"]`,
				{class: {active: route === state.router.page}},
				i18n.routes[route] || str.capitalize(route)
			)])
		))
	// ul('.menu.right', [
	// 	li(a(`[href="#/auth"]`, 'Login/Register'))
	// ])
]);
