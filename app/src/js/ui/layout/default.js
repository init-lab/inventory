'use strict';

// dom
const {
	h1, a, div,
	section, button, span
} = require('iblokz-snabbdom-helpers');

module.exports = ({state, actions, page}) => ([
	section('.content', [
		page({state, actions})
	])
]);
