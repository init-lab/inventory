'use strict';

// dom
const {
	h1, a, div,
	section, button, span
} = require('iblokz-snabbdom-helpers');

// components
const header = require('./header');
const breadcrumb = require('./breadcrumb');

const i18n = {
	routes: {
		home: 'Начало',
		items: 'Вещи',
		categories: 'Категории',
		locations: 'Локации',
		rooms: 'Стаи',
		owners: 'Собственици'
	},
	docs: {
		item: 'Вещ',
		category: 'Категория',
		location: 'Локация',
		room: 'Стая',
		owner: 'Собственик'
	},
	crud: {
		new: 'Добави',
		create: 'Добави',
		edit: 'Промени',
		delete: 'Изтрий',
		actions: 'Действия',
		cancel: 'Отказ',
		clear: 'Изчисти',
		save: 'Запази'
	},
	model: {
		rooms: {fields: {
			name: 'Име',
			image: 'Снимка'
		}},
		locations: {fields: {
			name: 'Име',
			address: 'Адрес'
		}},
		owners: {fields: {
			name: 'Име'
		}},
		categories: {fields: {
			name: 'Име',
			parent: 'Родител'
		}},
		items: {
			fields: {
				name: 'Име',
				category: 'Категория',
				owner: 'Собственик',
				room: 'Стая',
				placeInRoom: 'Място в стаята',
				size: 'Размер',
				dim: 'w:XX d:YY h:ZZ',
				amount: 'Бройка',
				images: 'Снимки'
			},
			values: {
				size: {
					1: 'дребно',
					2: 'една ръка',
					3: 'две ръце',
					4: 'двама души',
					5: 'трима души'
				}
			}
		}
	}
};

module.exports = ({state, actions, page}) => ([
	header({state, actions, i18n}),
	breadcrumb({state, actions, i18n}),
	section('.content', [
		page({state, actions, i18n})
	])
]);
