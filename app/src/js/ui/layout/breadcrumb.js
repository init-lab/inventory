// dom
const {
	h1, a, div, ul, li,
	section, button, span
} = require('iblokz-snabbdom-helpers');

const {str} = require('iblokz-data');

const getDocName = ({page, i18n}) => page.split('.').slice(-1)
	.map(str.pluralToSingular)
	.map(doc => i18n.docs[doc.toLowerCase()] || doc)
	.pop();

module.exports = ({state, actions, i18n = {routes: {}, crud: {}}}) => ul('.breadcrumb', [].concat(
	state.router.page !== 'home' ? {title: i18n.routes.home || 'Home', href: '#/'} : [],
	state.router.path.map((chunk, i) => (i < state.router.path.length - 1)
		? {
			title: i18n.routes[chunk] || str.capitalize(chunk),
			href: '#/' + state.router.page.split('.').slice(0, i + 1).join()
		}
		: state.router.pageId && !state.router.page.match(/configure/)
			? (state.router.pageId === 'new')
				? `${i18n.crud.new || 'New'} ${getDocName({page: state.router.page, i18n})}`
				: (state.rest.collections[state.router.page]
					&& state.rest.collections[state.router.page].schema
					&& state.rest.collections[state.router.page].schema.name
					&& state[state.router.page].doc.name)
						? state[state.router.page].doc.name
						: `${i18n.crud.edit || 'Edit'} ${getDocName({page: state.router.page, i18n})}: ${state.router.pageId}`
			: i18n.routes[chunk] || str.capitalize(chunk)
	)).map((item, i) =>
		li(typeof item === 'string'
			? item
			: a(`[href="${item.href}"]`, item.title)
		)
));
