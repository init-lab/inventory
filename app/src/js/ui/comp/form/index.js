'use strict';

// dom
const {
	h1, a, div, header,
	section, button, span,
	ul, li, br,
	table, thead, tbody, tr, th, td,
	form, input, label, fieldset, legend,
	select, option, textarea
} = require('iblokz-snabbdom-helpers');

// lib
const moment = require('moment');
const {obj, str, arr} = require('iblokz-data');
const formUtil = require('../../../util/form');

// fieldTypes
const file = require('./file');
const multiple = require('./multiple');

const sub = (o, p, d = false) =>
	o === undefined
		? d
		: (p instanceof Array)
			? o[p[0]] && p.length > 1
				? [sub(o[p[0]], p.slice(1))].map(sO => (sO !== undefined ? sO : d)).pop()
				: o[p[0]]
			: o[p] !== undefined ? o[p] : d;

// const schema = {
// 	name: {type: 'String', min: 3, max: 30},
// 	type: {type: 'String', enum: ['polygon', 'circle'], default: 'polygon'},
// 	path: [{
// 		lat: 'Number',
// 		lng: 'Number'
// 	}],
// 	center: {
// 		lat: 'Number',
// 		lng: 'Number'
// 	},
// 	radius: 'Number'
// };

const schemaInputFieldMap = {
	String: 'text',
	Number: 'number',
	Date: 'datetime-local',
	Boolean: 'checkbox'
};

const keys = o => Object.keys(o);

const decorateField = field => obj.switch(field.type, {
	default: field,
	String: ['content', 'article', 'post', 'text'].indexOf(field.name) > -1
		? {...field, type: 'RichText'}
		: field
});

const parseField = (field, name) => (
	console.log(field, name),
	(typeof field === 'string')
		? {type: field, name, required: true}
		: field.type
			? Object.assign({}, field, {name})
			: field instanceof Array
				? (field[0].isFile)
					? {type: 'File', name}
					: {type: 'Array', name, schema: parseField(field[0])}
				: field instanceof Object
					? {type: 'Object', name, fields: keys(field).map(key => parseField(field[key], [name, key].join('.')))}
					: false
);

//  keys(field).map(key => parseField(field[key], [name, key].join('.')))

const fieldToTitle = field => str.fromCamelCase(field, '-')
	.split('-').map(word => str.capitalize(word)).join(' ');

const formElement = ({field, doc, update = () => {}, focused = false, addons = []}) => (
	console.log(field, doc),
	obj.switch(field.type, {
		File: value => file({field, value, update, focused}),
		Mixed: value =>
			textarea(
				{attrs: {name: field.name, disabled: field.disabled}},
				typeof value === 'string' ? value : JSON.stringify(value)
			),
		Array: value => field.schema.type === 'String'
			? multiple({field, value})
			: formElement({...field, type: 'mixed'}, doc),
		Object: () => div('.field', [].concat(
			label([
				field.title || fieldToTitle(field.name.split('.').pop()),
				': '
			]),
			field.fields.map(f => formElement(f, doc))
		)),
		default: value => div('.field', [
			field.name.split('.').length === 1
				? label([
					field.title || fieldToTitle(field.name.split('.').pop()),
					': '
				]) : '',
			(field.type === 'String' && field.enum instanceof Array)
				? select({
					on: {change: ev => update(field.name.split('.'), ev.target.value)},
					attrs: {name: field.name, disabled: field.disabled}
				}, [].concat(
					(console.log(field), []),
					[].concat(
						'',
						field.enum
					).map((val, index) => option({
						attrs: {
							value: val,
							selected: typeof value === 'undefined' && index === 0 || val === value
						},
						props: {
							value: val,
							selected: typeof value === 'undefined' && index === 0 || val === value
						}
					}, val === '' ? 'n/a' : (field.enumLabels[val] || val))
					)))
				: (field.type === 'ObjectID' && field.hash instanceof Object)
					? select({
						on: {change: ev => update(field.name.split('.'), ev.target.value)},
						attrs: {name: field.name, disabled: field.disabled}
					}, [].concat(
						// option({'': 'n/a'}, 'n/a'),
						Object.keys(field.hash).map((id, index) =>
							option({
								attrs: {
									value: id,
									selected: typeof value === 'undefined' && index === 0 || id === value
								},
								props: {
									value: id,
									selected: typeof value === 'undefined' && index === 0 || id === value
								}
							}, field.hash[id]
							)
						)))
					: input({
						on: {input: ev => update(field.name.split('.'), ev.target.value)},
						attrs: {
							name: field.name,
							type: field.name.match('password') ? 'password' : schemaInputFieldMap[field.type],
							step: 0.01,
							placeholder: field.title || fieldToTitle(field.name.split('.').pop()),
							checked: field.type === 'Boolean' && value === true,
							disabled: field.disabled
						},
						props: {
							value: field.type === 'Date'
								? moment(value || field.default).format('YYYY-MM-DDTHH:mm')
								: value || field.default || ''
						}
					})
		])
	})((console.log(doc, field), sub(doc, field.name.split('.'), '')))
);

module.exports = ({schema, doc = {}, sel = '',
	noactions = false, actions: {save, cancel, update = () => {}},
	focused = false,
	addons = [],
	i18n = {crud: {}, model: {}}, model = ''
}, childNodes = []) => form(sel, {
	on: {
		submit: ev => {
			ev.preventDefault();
			let data = formUtil.toData(ev.target);
			Object.keys(data).forEach(key => {
				if (schema[key] && schema[key].hash && data[key] === '') {
					data[key] = null;
				}
			});
			save({data, ev});
		}
	}
}, [].concat(
	keys(schema)
		.map(key => parseField(schema[key], key))
		// .map(decorateField)
		.map(field => ({...field,
			...((i18n.model[model] && i18n.model[model].fields[field.name]) ? {
				title: i18n.model[model].fields[field.name]
			} : {}),
			...((field.enum && i18n.model[model] && i18n.model[model].values && i18n.model[model].values[field.name]) ? {
				enum: field.enum,
				enumLabels: i18n.model[model].values[field.name]
			} : {})
		}))
		.reduce((els, field) => [].concat(
			els,
			formElement({field, doc, update, focused: focused === field.name, addons})
			// hr()
		), []),
	!noactions ? [
		button(`.btn.btn-primary[type="sumbit"]`, i18n.crud.save || 'Save'),
		' ',
		button(`.btn.btn-white[type="button"]`, {on: {click: ev => cancel({ev})}}, i18n.crud.cancel || 'Cancel')
	] : [],
	childNodes
));
