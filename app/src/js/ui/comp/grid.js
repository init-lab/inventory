'use strict';

// dom
const {
	h1, a, div, header,
	section, button, span,
	ul, li, i, select, option, form,
	table, thead, tbody, tr, th, td
} = require('iblokz-snabbdom-helpers');

const log = msg => arg => (console.log(msg, arg), arg);

const parseFieldName = i18nFields => field => ({
	name: field.name || field,
	title: i18nFields[field.name || field] || field.title || field
});

const toggleSort = (sort, field) => sort.find(f => f.match(field))
	? (sort.find(f => f.match(field))[0] === '-')
		? sort.filter(f => !f.match(field))
		: sort.map(f => f.match(field) ? `-${f}` : f)
	: [].concat(sort, field);

module.exports = ({fields = [], list = [], actions = [], schema = {}, model = '', i18n = {crud: {}, model: {}},
	query = {start: 0, limit: 10, total: 0, sort: []}, load = query => {}}) =>
	div('.grid', [
		table([
			thead(
				tr([].concat(
					fields
						.map(parseFieldName(i18n.model[model] ? i18n.model[model].fields : []))
						.map(field => th({
							on: {click: () => load({...query, sort: toggleSort(query.sort, field.name)})}
						}, [
							field.title,
							i(`.sort.fa.fa-sort${
								query.sort.filter(f => f.match(field.name)).map(f => f[0] === '-' ? '-desc' : '-asc').pop() || ''
							}`)
						])),
					th(`[width=126]`, i18n.crud.actions || 'actions')
				))
			),
			tbody(list.map(doc =>
				tr([].concat(
					fields
						.map(field => ({field: field.name || field, value: doc[field.name || field] || ''}))
						// .map(log('value: '))
						.map(({field, value}) => ({
							field,
							value: value instanceof Array ? value.join(', ') : value
						}))
						.map(({field, value}) => td(
							schema[field] && schema[field].hash
								? schema[field].hash[value]
								: (schema[field].enum && i18n.model[model]
									&& i18n.model[model].values && i18n.model[model].values[field])
									? i18n.model[model].values[field][value]
									: value
						)),
					td(actions.map(a =>
						a.href
							? a(`${a.href}${a.sel || ''}`, {
								style: {
									width: (100 / actions.length).toFixed(2) + '%'
								}
							}, a.title || '')
							: button(`${a.sel || ''}`, {
								style: {
									width: (100 / actions.length).toFixed(2) + '%'
								},
								on: {
									click: ev => a.cb({ev, doc})
								}
							}, a.title || '')
					))
				))
			))
		]),
		form('.form.list-limit', [
			div('.field', select({
				on: {
					change: ev => load({...query, limit: ev.target.value})
				}
			}, [10, 20, 50, 100].map(
				limit => option({attrs: {
					value: limit,
					selected: limit === query.limit
				}}, limit)
			)))
		]),
		ul('.pagination', new Array(Math.ceil(query.total / query.limit)).fill({}).map((_empty, index) =>
			li({class: {active: index * query.limit === query.start}},
				a({on: {click: () => load({...query, start: index * query.limit})}}, index))
		))
	]);
