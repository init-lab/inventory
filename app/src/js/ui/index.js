'use strict';

// dom
const {
	h1, a, div,
	section, button, span
} = require('iblokz-snabbdom-helpers');

// util
const {obj, fn} = require('iblokz-data');

// layouts
const layout = {
	default: require('./layout/default'),
	dashboard: require('./layout/dashboard')
};

// pages
const pages = {
	default: require('./pages/home'),
	auth: require('./pages/auth'),
	// items: require('./pages/items'),
	// categories: require('./pages/categories'),
	resources: require('./pages/resources')
	// configure: require('./pages/configure')
};

module.exports = ({state, actions}) => section('#ui', [].concat(
	fn.pipe(
		() => state.rest.routes.api[state.router.path[0]]
			? pages.resources
			: obj.switch(state.router.path, pages),
		page => state.router.path[0] === 'auth'
			? layout.default({state, actions, page})
			: layout.dashboard({state, actions, page})
	)()
));
