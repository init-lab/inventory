'use strict';

// dom
const {
	h1, a, div, header, p,
	section, button, span, ul, li
} = require('iblokz-snabbdom-helpers');

const {obj, fn} = require('iblokz-data');

// crud
const list = require('./list');
const edit = require('./edit');

const toHash = (list, key, value) =>
	list.reduce((hash, el) => ({...hash, [el[key]]: el[value]}), {'': 'n/a'});

const processSchema = (schema, state) => obj.map(schema,
	(key, value) => value instanceof Object
		? value.type && value.type === 'ObjectID' && typeof value.hashMap !== 'undefined'
			? ({...value,
				hash: state[value.hashMap[0]].hash && Object.keys(state[value.hashMap[0]].hash).length > 0
					? ({'': 'n/a', ...state[value.hashMap[0]].hash})
					: toHash(state[value.hashMap[0]].list, ...value.hashMap.slice(1))
			})
			: value
		: value
);

module.exports = ({state, actions, i18n = {routes: {}, docs: {}, crud: {}, model: {}}}) => section('.resources', fn.pipe(
	() => state.rest.collections[state.router.path[0]].schema,
	rawSchema => processSchema(rawSchema, state),
	schema => [].concat(
		!state.router.pageId
			? list({state, actions, i18n, schema})
			: edit({state, actions, i18n, schema})
	)
)());
