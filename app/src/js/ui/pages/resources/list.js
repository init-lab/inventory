'use strict';

// dom
const {
	h1, a, div, header, p, i, img,
	section, button, span, ul, li
} = require('iblokz-snabbdom-helpers');
// components
const grid = require('../../comp/grid');
const form = require('../../comp/form');
const file = require('../../../util/file');

const {obj, str, fn} = require('iblokz-data');

const orderWeight = [
	'_id', 'name', 'title',
	'function', 'type',
	'values', 'default',
	'author', 'start', 'end'
];

const skipFields = ['description', 'info'];

const schemaToFields = schema => ['_id'].concat(Object.keys(schema))
	.sort((a, b) => orderWeight[b] - orderWeight[a])
	.map(field => obj.switch(field, {
		default: () => field,
		_id: () => ({name: '_id', title: '#'})
	})());

const getDocName = ({page, i18n}) => page.split('.').slice(-1)
	.map(str.pluralToSingular)
	.map(doc => i18n.docs[doc.toLowerCase()] || doc)
	.pop();

const parseFieldName = i18nFields => field => ({
	name: field.name || field,
	title: i18nFields[field.name || field] || field.title || field
});

const toCSV = (fields, list, schema = {}, i18nModel = {}) => [].concat(
	fields.map(field => `"${field.title || field}"`).join(','),
	list.map(row => fields
		.map(field => schema[field.name || field]
			? schema[field.name || field].hash
				? schema[field.name || field].hash[row[field.name || field]]
				: schema[field.name || field].enum && i18nModel.values
					? i18nModel.values[field.name || field][row[field.name || field]]
					: row[field.name || field]
			: row[field.name || field])
		.map(val => typeof val === "string" && val.match(/,/)
			? `"${val}"` : val
		)
		.join(','))
).join("\n");

module.exports = ({state, actions, schema, i18n = {crud: {}, docs: {}, model: {}}}) => fn.pipe(
	() => state.router.path[0],
	collection => [].concat(
		button('.btn', {on: {click: () => actions.router.go(`${collection}.new`)}}, [
			i('.fa.fa-file-o'),
			`${i18n.crud.create || 'Create'} ${getDocName({page: state.router.page, i18n})}`
		]),
		span('.right', [
			button('.btn[title="Експортирай .csv файл"]', {
				on: {
					click: () => file.save(`${collection}.csv`, toCSV(schemaToFields(schema),
						state[collection].list,
						schema,
						i18n.model[collection] || {}
					))
				}
			}, [
				// img(`[src="/assets/csv-file.svg"]`),
				'.csv',
				i('.fa.fa-download')
			])
			// button('.btn[title="Импортирай .csv файл"]', {}, [
			// 	// img(`[src="/assets/csv-file.svg"]`),
			// 	'.csv',
			// 	i('.fa.fa-upload')
			// ])
		]),
		// filters
		state.rest.views[collection] && state.rest.views[collection].list.filters
			? form({
				i18n, model: collection,
				sel: '.filters',
				noactions: true,
				schema: state.rest.views[collection].list.filters
					.reduce((newSchema, field) => ({...newSchema, [field]: schema[field]}), {}),
				doc: state[collection].query,
				actions: {
					update: (field, value) => (
						console.log('filter', field, value),
						actions[collection].list({...state[collection].query, [field]: value})
					)
				}
			}, [button('.btn', {on: {click: () =>
				actions[collection].list(obj.filter(state[collection].query, key =>
					!state.rest.views[collection].list.filters.find(f => f === key)))
			}}, [
				`${i18n.crud.clear || 'Clear'}`
			])])
			: [],
		grid({
			model: collection,
			i18n,
			fields: state.rest.views && state.rest.views[collection]
				? state.rest.views[collection].list.columns
				: schemaToFields(schema),
			list: state[collection].list,
			schema,
			query: state[collection].query,
			load: query => actions[collection].list(query),
			actions: [
				{
					sel: '.fa.fa-eye',
					cb: ({doc, ev}) => {}
				},
				{
					sel: '.fa.fa-pencil',
					cb: ({doc, ev}) => actions.router.go(`${collection}.${doc._id}`)
				},
				{
					sel: '.fa.fa-trash-o',
					cb: ({doc, ev}) => actions[collection].delete(doc._id, null,
						() => actions[collection].list(state[collection].query)
					)
				}
			]
		})
	)
)();
