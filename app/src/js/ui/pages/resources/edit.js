'use strict';

// dom
const {
	h1, a, div, header, p,
	section, button, span, ul, li
} = require('iblokz-snabbdom-helpers');
// components
const form = require('../../comp/form');

// lib
const {obj} = require('iblokz-data');

const excludeFields = [
	'createdBy', 'createdOn'
];

module.exports = ({state, actions, schema, i18n = {crud: {}, model: {}}}) => form({
	i18n, model: state.router.path[0],
	sel: state.router.pageId === 'new' ? '.create' : '.edit',
	schema: obj.filter(
		schema,
		(field, value) => (
			// console.log(field, excludeFields),
			excludeFields.indexOf(field) === -1
		)
	),
	doc: ({
		...obj.filter(state[state.router.path[0]].query,
			key => state.rest.views[state.router.path[0]].list.filters
				&& state.rest.views[state.router.path[0]].list.filters.find(k => k === key)),
		...state[state.router.path[0]].doc
	}),
	focused: state[state.router.path[0]].focused || false,
	actions: {
		update: (field, value) => (
			console.log('update', field, value),
			actions.set([state.router.path[0]], {
				focused: field[0],
				doc: {
					...state[state.router.path[0]].doc,
					[field]: value
				}
			})
		),
		save: ({data, ev}) => (
			actions[state.router.path[0]].save(
				obj.map(
					Object.assign(state[state.router.path[0]].doc, data),
					(key, value) => ['content', 'article', 'post', 'text'].indexOf(key) > -1
						? value.content : value
				),
				null,
				() => actions.router.go(state.router.path.slice(0, 1).join('.'))
			)
		),
		cancel: ({ev}) => actions.router.go(state.router.path.slice(0, 1).join('.'))
	}
});
