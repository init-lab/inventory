'use strict';

// dom
const {
	h1, a, div, header, p, img,
	section, button, span, i, hr, br,
	fieldset, legend, form, input, label
} = require('iblokz-snabbdom-helpers');

module.exports = ({state, actions}) => section('.auth', [
	h1([
		img(`[src="assets/logo.png"]`),
		// i('.fa.fa-puzzle-piece'),
		' initLab Inventory'
	]),
	section('.forms', [
		fieldset('.login', [
			legend('Login'),
			form('.form', {
				on: {
					submit: ev => (
						ev.preventDefault(),
						actions.router.go('home')
					)
				}
			}, [
				div('.field', [
					label('Email'),
					input(`[name="email"]`)
				]),
				div('.field', [
					label('Password'),
					input(`[name="password"][type="password"]`)
				]),
				br(),
				div('.field',
					button('Login')
				)
			])
		]),
		fieldset('.register', [
			legend('Register'),
			form('.form', {
				on: {
					submit: ev => (
						ev.preventDefault(),
						actions.router.go('home')
					)
				}
			}, [
				div('.field', [
					label('Full Name'),
					input(`[name="fullName"]`)
				]),
				div('.field', [
					label('Email'),
					input(`[name="email"]`)
				]),
				div('.field', [
					label('Password'),
					input(`[name="password"][type="password"]`)
				]),
				br(),
				div('.field',
					button('Register')
				)
			])
		])
	])
]);
