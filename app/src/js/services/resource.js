'use strict';

const Rx = require('rx');
const $ = Rx.Observable;
const {Subject} = Rx;
const path = require('path');

const {obj} = require('iblokz-data');
const request = require('superagent');
const app = require('../util/app');
// const auth = require('./auth');

const port = ''; // process.env.PORT && process.env.PORT !== '80' ? `:${process.env.PORT}` : '';
const url = `//${location.host || process.env.SERVER_URL || 'localhost'}${port}`;

const ifThen = res => res ? Promise.resolve(res) : Promise.reject(res);

const initial = {
	query: {
		start: 0,
		limit: 10,
		total: 0,
		sort: []
	},
	list: [],
	hash: {},
	doc: {},
	focused: false,
	dirty: true
};

const reservedPageIds = ['new', 'configure'];
// const anyOf = (list, filter)

const handleUnauthorized = err => (
	console.log(err),
	[401, 403].indexOf(err.status) > -1
		? state => state // auth.actions.forceLogout()
		: state => state
);

const apiList = ns => (query = initial.query, ctrl = '', token) => request.get(`${url}/api/${path.join(ns, ctrl)}`)
	// .set(token && {'x-access-token': token} || {})
	.query(query)
	.then(res => res.body);

// ns - namespace
const list = ns => (query = initial.query, ctrl = '', token) => apiList(ns)(query, ctrl, token)
	.then(data => state => obj.patch(state, ns, {
		list: data.list,
		query: Object.assign({}, query, {
			start: data.start,
			limit: data.limit,
			total: data.total,
			sort: data.sort || query.sort
		}),
		doc: {},
		focused: false,
		dirty: true
	}))
	.catch(handleUnauthorized);

const hash = ns => ([_ns, key, value], query = {limit: 1000, start: 0}, token) => apiList(ns)(query, '', token)
	.then(data => data.list)
	.then(list => list.reduce((hash, el) => Object.assign({}, hash, {[el[key]]: el[value]}), {}))
	.then(hash => state => obj.patch(state, ns, {hash}));

const query = ns => keyValuePair => state => obj.patch(state, ns, {
	query: Object.assign({}, obj.sub(state, [].concat(ns, 'query')), keyValuePair),
	dirty: true
});

const create = ns => (doc, token, cb = () => {}) => request.post(`${url}/api/${ns}`)
	// .set(token && {'x-access-token': token} || {})
	.send(doc)
	.then(() => (
		cb(),
		state => obj.patch(state, ns, {dirty: true, doc: {}})
	))
	.catch(handleUnauthorized);

const read = ns => (id, token) => request.get(`${url}/api/${ns}/${id}`)
	// .set(token && {'x-access-token': token} || {})
	.then(res => res.body)
	.then(doc => state => obj.patch(state, ns, {doc}))
	.catch(handleUnauthorized);

const update = ns => (id, doc, token, cb = () => {}) => request.put(`${url}/api/${ns}/${id}`)
	// .set(token && {'x-access-token': token} || {})
	.send(doc)
	.then(() => (
		cb(),
		state => obj.patch(state, ns, {dirty: true, doc: {}})
	))
	.catch(handleUnauthorized);

const save = ns => (doc, token, cb = () => {}) => doc._id
	? update(ns)(doc._id, doc, token, cb)
	: create(ns)(doc, token, cb);

const _delete = ns => (id, token, cb = () => {}) => request.delete(`${url}/api/${ns}/${id}`)
	// .set(token && {'x-access-token': token} || {})
	.then(() => (
		cb(),
		state => obj.patch(state, ns, {dirty: true, doc: {}})
	))
	.catch(handleUnauthorized);

const reset = ns => () => state => obj.patch(state, ns, {doc: {}});

const filter = ns => query => state => obj.patch(state, [ns, 'query'], query);

const actions = {
	initial,
	list,
	hash,
	query,
	create,
	update,
	save,
	delete: _delete,
	read,
	reset,
	filter
};

const applyNs = (o, ns) => obj.map(o, (k, v) => (v instanceof Function) ? v(ns) : v);

const attach = (o, ns) => app.attach(o, ns, applyNs(actions, ns));
// obj.patch(o, ns, applyNs(actions, ns));

let subs = [];
let unhook = () => subs.forEach(sub => sub.dispose());

const hook = ({state$, actions}) => ns => {
	// load on login
	subs.push(
		state$
			.distinctUntilChanged(state => [state[ns].dirty]) // , state.auth.user])
			.filter(state => state[ns].dirty) // && state.auth.user)
			.subscribe(state => (
				actions[ns].list(state[ns].query, ''),
				ifThen(state.rest.hashes && state.rest.hashes.find(el => el[0] === ns))
					.then(hashMap => actions[ns].hash(hashMap))
			)) // , state.auth.token))
	);
	// on edit load doc
	subs.push(
		state$
			.distinctUntilChanged(state => state.router.path)
			.filter(state => (
				state.router.path[0] === ns
			))
			.subscribe(state =>
				[null, ''].indexOf(state.router.pageId) > -1 || reservedPageIds.find(
					pageId => state.router.pageId.match(pageId)
				)
					? [null, ''].indexOf(state.router.pageId) > -1
						|| state.router.pageId.split('/').length === 1
						|| state.router.pageId === 'new'
						? actions[ns].list(state[ns].query)
						: true
					: actions[ns].read(
						state.router.path.slice().pop()
						// state.auth.token
					)
			)
	);
};

module.exports = {
	actions,
	applyNs,
	attach,
	hook,
	unhook: () => unhook()
};
